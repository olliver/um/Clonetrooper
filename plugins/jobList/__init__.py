from .jobAPI import JobAPI
from .jobQueueSubmit import JobQueueSubmit
from .jobStateUpdate import JobStateUpdate
from .jobRecordToApiOutput import JobRecordToApiOutput

def register():
    return [
        {"type": "api", "class": JobAPI},
        {"type": "background", "class": JobQueueSubmit},
        {"type": "background", "class": JobStateUpdate},
        {"type": "converter", "class": JobRecordToApiOutput}
    ]
