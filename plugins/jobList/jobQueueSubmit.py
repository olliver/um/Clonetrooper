from ct.daemon import background
from ct.core import database
import re
import logging

log = logging.getLogger(__name__.split(".")[-1])


class JobQueueSubmit(background.BackgroundTask):
    def __init__(self):
        # When we are starting up, check if any printers/jobs are in limbo state.
        # Limbo state happens when a job is being send to a printer, but it was never started properly.
        # (Happens if code crashes or system restarts)
        with database.Session() as s:
            for printer in s.query(database.Printer).join(database.PrintJob).filter(database.PrintJob.uuid == None).all():
                log.warning("Recovering job %d from limbo state", printer.current_job.id)
                printer.current_job.state = "not_started"
                printer.current_job = None

    def run(self):
        printer_id, job_id = self.__findJobToStart()

        if printer_id is None or job_id is None:
            return

        self.__startJob(printer_id, job_id)

    def __findJobToStart(self):
        with database.Session() as s:
            for printer in s.query(database.Printer).filter_by(state="idle").filter(database.Printer.current_job_id == None).all():
                # Got an idle printer, find a not started print job
                for job in s.query(database.PrintJob).filter_by(state="not_started").all():
                    if self.__canStartJobOnPrinter(s, job, printer):
                        return printer.id, job.id
        return None, None

    def __startJob(self, printer_id, job_id):
        # Update the status of this job to sending to a printer.
        with database.Session() as s:
            job = s.query(database.PrintJob).filter_by(id=job_id).one()
            try:
                file_handle = open(job.filename, "rb")
            except FileNotFoundError:
                log.error("File not found for job: %s", job.filename)
                job.state = "FileNotFoundError"
                return
            job.state = "sending"
            printer = s.query(database.Printer).filter_by(id=printer_id).one()
            printer.current_job = job
            s.commit()

            api = printer.api()
            log.info("Going to send job [%s] to [%s]", job.filename, printer.name)
            try:
                result = api.post("api/v1/print_job", files={"file": (job.name, file_handle)}, timeout=60)
            except IOError:
                log.exception("IOError during sending of a printjob")
                result = None
            else:
                log.info("Job send result: %s", result.content)

            if result is None or result.status_code != 201:
                # Failed to start, could be a number of reasons. But we need to place the job back into the queue.
                job.state = "not_started"
                printer.current_job_id = None
            else:
                job.state = "sent"
                job.uuid = result.json()["uuid"]
            s.commit()

    def __canStartJobOnPrinter(self, s, job, printer):
        job_hotend_count = self.__getHotendCount(job)
        if printer.getPropertyValue("HOTEND_COUNT") is None:
            return False
        if int(printer.getPropertyValue("HOTEND_COUNT")) < job_hotend_count:
            return False
        for hotend_index in range(job_hotend_count):
            if abs(self.__getNozzleSize(printer.getPropertyValue("HOTEND[%d]TYPE_ID" % hotend_index)) - float(job.getPropertyValue("EXTRUDER_TRAIN.%d.NOZZLE.DIAMETER" % (hotend_index)))) > 0.01:
                return False
            if not self.__isMaterialMatch(s, printer.getPropertyValue("HOTEND[%d]MATERIAL" % hotend_index), job.getPropertyValue("EXTRUDER_TRAIN.%d.MATERIAL.GUID" % (hotend_index))):
                return False
        return True

    def __getHotendCount(self, job):
        n = 0
        while True:
            if job.getPropertyValue("EXTRUDER_TRAIN.%d.NOZZLE.DIAMETER" % (n)) is None:
                return n
            n += 1

    def __getNozzleSize(self, hotend_id):
        if hotend_id is None:
            return 0.0
        m = re.fullmatch("[A-Z][A-Z] ([0-9]*\.[0-9]*)", hotend_id)
        if m is not None:
            return float(m.group(1))
        m = re.fullmatch("([0-9]*\.[0-9]*)mm", hotend_id)
        if m is not None:
            return float(m.group(1))
        return 0.0

    def __isMaterialMatch(self, s, material_guid1, material_guid2):
        if material_guid1 is None or material_guid2 is None:
            return True
        if material_guid1 == material_guid2:
            return True
        material1 = s.query(database.Material).filter_by(guid=material_guid1).first()
        if material1 is None:
            return False
        material2 = s.query(database.Material).filter_by(guid=material_guid2).first()
        if material2 is None:
            return False
        return material1.material == material2.material
