from ct.daemon import background
from ct.core import database
import os


class JobStateUpdate(background.BackgroundTask):
    def run(self):
        with database.Session() as s:
            for printer, job in s.query(database.Printer, database.PrintJob).filter(database.Printer.current_job_id == database.PrintJob.id).filter(database.PrintJob.uuid != None).all():
                remote_api = printer.api()
                try:
                    print_job_response = remote_api.get("api/v1/print_job")
                except IOError:
                    pass
                else:
                    json = print_job_response.json()
                    if print_job_response.status_code != 404 and json["uuid"] == job.uuid:
                        job.state = json["state"]
                    else:
                        self.__jobFinished(job)
                        s.commit()

    def __jobFinished(self, job):
        job.state = "finished"
        job.active_printer.current_job_id = None
        os.remove(job.filename)
