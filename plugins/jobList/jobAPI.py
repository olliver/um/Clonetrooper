from ct.core.converter import Converter
from ct.core import database
from ct.server import api
from ct import config
from . import gcodePropertyParser
import os
import flask
import werkzeug
import logging
import random
import sqlalchemy.orm

log = logging.getLogger(__name__.split(".")[-1])


class JobAPI(api.ApiPlugin):
    @api.endpoint("jobs", "get")
    def getJobs(self):
        data = []
        with database.Session() as s:
            for job in s.query(database.PrintJob).options(sqlalchemy.orm.joinedload(database.PrintJob.active_printer), sqlalchemy.orm.joinedload(database.PrintJob.properties)).all():
                data.append(Converter.convert(job, dict))
        return api.response(data)

    @api.endpoint("jobs/<int:job_id>", "get")
    def getJob(self, job_id):
        with database.Session() as s:
            job = s.query(database.Printer).filter_by(id=job_id).first()
            if not job:
                return api.response({}, 404)
            return api.response(Converter.convert(job, dict))

    @api.endpoint("jobs", "post")
    def postJob(self):
        received_file = flask.request.files.get("file")
        if not received_file:
            log.warning("No file received")
            return api.failedResponse()
        filename = received_file.filename
        if not filename.endswith(".gcode.gz") and not filename.endswith(".gcode"):
            log.warning("File is not gcode: %s", filename)
            return api.failedResponse()

        storage_filename = werkzeug.secure_filename(received_file.filename)
        file_path = os.path.join(config.FILE_STORAGE_PATH, storage_filename)
        jobname = received_file.filename
        os.makedirs(config.FILE_STORAGE_PATH, exist_ok=True)
        while os.path.isfile(file_path):
            storage_filename = random.choice("abcdefghijklmnopqurtuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ") + storage_filename
            file_path = os.path.join(config.FILE_STORAGE_PATH, storage_filename)

        received_file.save(file_path)
        properties = gcodePropertyParser.GCodePropertyParser().getProperties(file_path)

        with database.Session() as s:
            job = database.PrintJob(name=jobname, filename=file_path, state="not_started")
            for k, v in properties.items():
                job.setPropertyValue(k, v)
            s.add(job)
            s.commit()
            job_id = job.id

        return api.response({"message": "YAY", "id": job_id}, 201)
