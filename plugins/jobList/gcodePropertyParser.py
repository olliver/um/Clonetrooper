import gzip
import os


class GCodePropertyParser:
    def __init__(self):
        self.__file = None

    def getProperties(self, filename):
        self.__openFile(filename)
        if not self.__findStart():
            self.__file.close()
            return {"ERROR": "No header"}
        properties = {}
        for line in self.__file:
            if not line.startswith(";"):
                self.__file.close()
                return {"ERROR": "Header malformed"}
            data = line[1:].strip().split(":", 1)
            if data[0] == "END_OF_HEADER":
                break
            properties[data[0]] = data[1]
        self.__file.close()
        return properties

    def __findStart(self):
        for n in range(0, 10):
            line = self.__file.readline()
            if line.startswith(";START_OF_HEADER"):
                return True
        return False

    def __openFile(self, filename):
        try:
            self.__file = gzip.open(filename, "rt")
            self.__file.readline()
            self.__file.seek(0, os.SEEK_SET)
        except OSError:
            self.__file = open(filename, "rt")
