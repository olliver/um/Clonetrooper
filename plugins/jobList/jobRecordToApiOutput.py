from ct.core import converter
from ct.core import database


class JobRecordToApiOutput(converter.Converter):
    def __init__(self):
        super().__init__(database.PrintJob, dict)

    def handleConversion(self, input):
        properties = {}
        for p in input.properties:
            properties[p.key] = p.value
        return {
            "id": input.id,
            "time_elapsed": 0,
            "time_remaining": 0,
            "name": input.name,
            "uuid": input.id,
            "printer": input.active_printer.guid if input.active_printer is not None else "",
            "state": input.state,
            "properties": properties
        }
