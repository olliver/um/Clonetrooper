from .printerAPI import PrinterAPI
from .printerStateUpdate import PrinterStateUpdate
from .printerRecordToApiOutput import PrinterRecordToApiOutput


def register():
    return [
        {"type": "api", "class": PrinterAPI},
        {"type": "background", "class": PrinterStateUpdate},
        {"type": "converter", "class": PrinterRecordToApiOutput}
    ]
