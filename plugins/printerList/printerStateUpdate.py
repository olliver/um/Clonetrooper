from ct.daemon import background
from ct.core import database


class PrinterStateUpdate(background.BackgroundTask):
    def run(self):
        with database.Session() as s:
            for printer in s.query(database.Printer).all():
                remote_api = printer.api()
                try:
                    printer_response = remote_api.get("api/v1/printer")
                    system_response = remote_api.get("api/v1/system")
                except IOError:
                    printer.state = "Offline"
                else:
                    printer_json = printer_response.json()
                    if "status" not in printer_json:
                        continue
                    printer.state = printer_json["status"]

                    hotend_count = len(printer_json["heads"][0]["extruders"])
                    printer.setPropertyValue("HOTEND_COUNT", hotend_count)
                    for hotend_index in range(hotend_count):
                        extruder = printer_json["heads"][0]["extruders"][hotend_index]
                        hotend = extruder["hotend"]
                        printer.setPropertyValue("HOTEND[%d]MATERIAL" % (hotend_index), extruder["active_material"]["guid"])
                        printer.setPropertyValue("HOTEND[%d]TYPE_ID" % (hotend_index), hotend["id"])

                        if extruder["active_material"]["guid"] != "":
                            if s.query(database.Material).filter_by(guid=extruder["active_material"]["guid"]).first() is None:
                                material_profile_string = remote_api.get("api/v1/materials/%s" % extruder["active_material"]["guid"]).json()
                                s.add(database.Material.fromXml(material_profile_string))

                    system_json = system_response.json()
                    if "guid" in system_json and (printer.guid == "" or printer.guid == "?"):  # Update the GUID if it is not set.
                        printer.guid = system_json["guid"]
                    if "name" in system_json:
                        printer.name = system_json["name"]
                s.commit()
