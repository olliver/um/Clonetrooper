from ct.core import converter
from ct.core import database


class PrinterRecordToApiOutput(converter.Converter):
    def __init__(self):
        super().__init__(database.Printer, dict)

    def handleConversion(self, input):
        properties = {}
        for p in input.properties:
            properties[p.key] = p.value
        return {
            "name": input.name,
            "guid": input.guid,
            "state": input.state,
            "ip_address": input.ip_address,
            "job": converter.Converter.convert(input.current_job, dict),
            "properties": properties
        }
