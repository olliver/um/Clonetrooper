from ct.core import database
from ct.core.converter import Converter
from ct.server import api
import flask


class PrinterAPI(api.ApiPlugin):
    @api.endpoint("printers", "get")
    def getPrinters(self):
        data = []
        with database.Session() as s:
            for printer in s.query(database.Printer).all():
                data.append(Converter.convert(printer, dict))
        return api.response(data)

    @api.endpoint("printers/<string:guid>", "get")
    def getPrinter(self, guid):
        with database.Session() as s:
            printer = s.query(database.Printer).filter_by(guid=guid).first()
            if not printer:
                return api.response({}, 404)
            return api.response(Converter.convert(printer, dict))

    @api.endpoint("printers/<string:guid>", "delete")
    def deletePrinter(self, guid):
        with database.Session() as s:
            printer = s.query(database.Printer).filter_by(guid=guid).first()
            if not printer:
                return api.response({}, 404)
            if printer.current_job_id is not None: # Do not delete printer when there is a job running on it.
                return api.failedResponse()
            s.delete(printer)
            return api.response({"message": "YAY"}, 200)

    @api.endpoint("printers", "post")
    def putPrinters(self):
        hostname = flask.request.form.get("hostname", None)
        ip_address = flask.request.form.get("ip", None)
        if (hostname is None or hostname.strip() == "") and (ip_address is None or ip_address.strip() == ""):
            return api.failedResponse()
        if hostname is not None and hostname.strip() != "" and ip_address is not None and ip_address.strip() != "":
            return api.failedResponse()

        with database.Session() as s:
            if hostname:
                discovered_printer = s.query(database.DiscoveredPrinter).filter_by(hostname=hostname).first()
                if discovered_printer is None:
                    return api.failedResponse()
            else:
                discovered_printer = s.query(database.DiscoveredPrinter).filter_by(ip_address=ip_address).first()
                if discovered_printer is None:
                    discovered_printer = database.DiscoveredPrinter(name="?", hostname="?", guid="?", ip_address=ip_address)
                    s.add(discovered_printer)
            discovered_printer.state = "auth_request"
        return api.response({"message": "YAY"}, 201)
