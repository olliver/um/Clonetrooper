from .avahiClusterBroadcast import AvahiClusterBroadcast

def register():
    return [
        {"type": "avahi", "class": AvahiClusterBroadcast},
    ]
