import logging
import platform

try:
    from ct import dbusif
except ImportError:
    dbusif = None

log = logging.getLogger(__name__.split(".")[-1])


# Register ourselves with on the Avahi server, which handles zeroconf.
# This allows the printer to be visible on the local network
class AvahiClusterBroadcast:
    SERVER_INVALID, SERVER_REGISTERING, SERVER_RUNNING, SERVER_COLLISION, SERVER_FAILURE = range(0, 5)
    ENTRY_GROUP_UNCOMMITED, ENTRY_GROUP_REGISTERING, ENTRY_GROUP_ESTABLISHED, ENTRY_GROUP_COLLISION, ENTRY_GROUP_FAILURE = range(0, 5)
    IF_UNSPEC = -1
    PROTO_UNSPEC, PROTO_INET, PROTO_INET6 = -1, 0, 1

    def __init__(self):
        if dbusif is None:
            return
        self.__service_name = platform.node()
        # List of each Mdns name that we expose ourselves as.
        self.__services = [
            {"type": "_um-cluster._tcp", "port": 81},
        ]

        self.__avahi_entry_group = None
        self.__avahi_server = None

        self.__txt_values = {}

        self.__setTxtValue("type", "Clonetrooper")
        self.__setTxtValue("name", "Clonetrooper-%s" % (self.__service_name[-6:].lstrip("0")))

        self.__avahi_server = dbusif.RemoteObject("org.freedesktop.Avahi", "/", "org.freedesktop.Avahi.Server")
        self.__avahi_server.connectSignal("StateChanged", self.__avahiServerStateChanged)
        self.__avahiServerStateChanged(self.__avahi_server.GetState(), "")

    def __avahiServerStateChanged(self, state, error):
        if state == self.SERVER_RUNNING:
            # Register ourselves on the Avahi service
            self.__avahi_entry_group = dbusif.RemoteObject("org.freedesktop.Avahi", self.__avahi_server.EntryGroupNew(), "org.freedesktop.Avahi.EntryGroup")
            self.__avahi_entry_group.connectSignal("StateChanged", self.__entryGroupStateChanged)
            for service_data in self.__services:
                self.__avahi_entry_group.AddService(
                    self.IF_UNSPEC,     # interface
                    self.PROTO_UNSPEC,  # protocol
                    0,                  # flags
                    self.__service_name, service_data["type"],
                    "", "",
                    service_data["port"],
                    self.__getServerTextDBusFormat())
            self.__avahi_entry_group.Commit()

    def __getServerTextDBusFormat(self):
        ret = []
        for key, value in self.__txt_values.items():
            ret.append(list(map(ord, "%s=%s" % (key, value))))
        return ret

    def __entryGroupStateChanged(self, state, error):
        if state == self.ENTRY_GROUP_ESTABLISHED:
            log.info("Registered service on avahi (%s)", self.__service_name)
        elif state == self.ENTRY_GROUP_FAILURE:
            log.warning("Failed to register service on avahi (%s)", self.__service_name)

    def __setTxtValue(self, key, value):
        if key in self.__txt_values and self.__txt_values[key] == value:
            return
        self.__txt_values[key] = value
        if self.__avahi_entry_group is not None:
            for service_data in self.__services:
                self.__avahi_entry_group.UpdateServiceTxt.callLater(
                    self.IF_UNSPEC,     # interface
                    self.PROTO_UNSPEC,  # protocol
                    0,                  # flags
                    self.__service_name, service_data["type"],
                    "",
                    self.__getServerTextDBusFormat())
