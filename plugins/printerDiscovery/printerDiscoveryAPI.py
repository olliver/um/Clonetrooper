from . import printerDiscoveryUpdate

from ct.core.converter import Converter
from ct.core import database
from ct.core.pluginRegistry import PluginRegistry
from ct.server import api


class PrinterDiscoveryAPI(api.ApiPlugin):
    @api.endpoint("discoveredprinters", "get")
    def getPrinters(self):
        data = []
        with database.Session() as s:
            for printer in s.query(database.DiscoveredPrinter).all():
                data.append(Converter.convert(printer, dict))
        return api.response(data)

    @api.endpoint("discoveredprinters/scan", "post")
    def scanForNewPrinters(self):
        PluginRegistry.getInstance().getPluginInstance(printerDiscoveryUpdate.PrinterDiscoveryUpdate).start()
        return api.response({"message": "YAY"}, 200)
