from .printerDiscoveryAPI import PrinterDiscoveryAPI
from .printerDiscoveryUpdate import PrinterDiscoveryUpdate
from .printerAuthenticationUpdate import PrinterAuthenticationUpdate
from .discoveredPrinterToApiOutput import DiscoveredPrinterToApiOutput


# The printer discovery handles finding of new printers on the network,
# As well as updating IP addresses of already added printers to the print cluster.
def register():
    return [
        {"type": "api", "class": PrinterDiscoveryAPI},
        {"type": "background", "class": PrinterDiscoveryUpdate},
        {"type": "background", "class": PrinterAuthenticationUpdate},
        {"type": "converter", "class": DiscoveredPrinterToApiOutput},
    ]
