from ct.daemon import background
from ct.core import database
from ct.ultimaker3api import Ultimaker3API


class PrinterAuthenticationUpdate(background.BackgroundTask):
    def run(self):
        with database.Session() as s:
            for printer in s.query(database.DiscoveredPrinter).filter_by(state="auth_request").all():
                remote_api = printer.api()
                try:
                    auth_request_response = remote_api.post("api/v1/auth/request", data={"application": "Clonetrooper", "user": "Supreme Chancellor Sheev Palpatine"})
                except IOError:
                    pass
                else:
                    data = auth_request_response.json()
                    printer.api_auth_id = data["id"]
                    printer.api_auth_key = data["key"]
                    printer.state = "auth_verify"
            for printer in s.query(database.DiscoveredPrinter).filter_by(state="auth_verify").all():
                remote_api = printer.api()
                try:
                    auth_check_response = remote_api.get("api/v1/auth/check/%s" % printer.api_auth_id)
                except IOError:
                    pass
                else:
                    data = auth_check_response.json()
                    if data["message"] == "authorized":
                        # Get all the materials known by this printer, so we have an initial list of materials.
                        for material_xml in remote_api.get("api/v1/materials").json():
                            material = database.Material.fromXml(material_xml)
                            if s.query(database.Material).filter_by(guid=material.guid).first() is None:
                                s.add(material)
                        # Authorization successful. Add this printer to the list of printers in this cluster.
                        new_printer = database.Printer(name=printer.name, guid=printer.guid, ip_address=printer.ip_address, state="Offline", api_auth_id=printer.api_auth_id, api_auth_key=printer.api_auth_key)
                        s.add(new_printer)
                        s.delete(printer)
                        s.commit()
                    elif data["message"] == "unauthorized":
                        printer.state = "auth_failed"
