from ct.daemon import background
from ct.core import database
from ct.ultimaker3api import Ultimaker3API
from ct import thread
try:
    from ct import dbusif
except ImportError:
    dbusif = None
#TODO: When we do not have dbusif or avahi is not running, we could fall back to the python-zeroconf package

import logging
import re

log = logging.getLogger(__name__.split(".")[-1])


class PrinterDiscoveryUpdate(background.BackgroundTask):
    IF_UNSPEC = -1
    PROTO_UNSPEC, PROTO_INET, PROTO_INET6 = -1, 0, 1

    __SERVICE_TYPE = "_ultimaker._tcp"

    def __init__(self):
        self.__discovered_services_lock = thread.RLock()
        self.__discovered_services = {}

        self.__avahi_server = None
        self.__service_browser = None

        self.start()

    def start(self):
        self.stop()
        if dbusif is None:
            return
        if self.__avahi_server is None:
            self.__avahi_server = dbusif.RemoteObject("org.freedesktop.Avahi", "/", "org.freedesktop.Avahi.Server")
        browser_path = self.__avahi_server.ServiceBrowserNew(self.IF_UNSPEC, self.PROTO_INET, self.__SERVICE_TYPE, "", 0)

        self.__service_browser = dbusif.RemoteObject("org.freedesktop.Avahi", browser_path, "org.freedesktop.Avahi.ServiceBrowser")
        self.__service_browser.connectSignal("ItemNew", self.__newService)
        self.__service_browser.connectSignal("ItemRemove", self.__removeService)

    def stop(self):
        if self.__service_browser is None:
            return
        self.__service_browser.Free()   # Tell Avahi we no longer need this service browser.
        self.__service_browser.cleanSignals()
        self.__service_browser = None

    def __newService(self, interface, protocol, hostname, type, domain, flags):
        # interface = IF_UNSPEC
        # protocol = PROTO_INET or PROTO_INET6
        # name = hostname
        # type = "_ultimaker._tcp"
        # domain = "local"
        # flags = ?
        with self.__discovered_services_lock:
            self.__discovered_services[hostname] = None

    def __removeService(self, interface, protocol, hostname, type, domain, flags):
        with self.__discovered_services_lock:
            if hostname in self.__discovered_services:
                del self.__discovered_services[hostname]
            self.__removePrinter(hostname)

    def run(self):
        with self.__discovered_services_lock:
            hostnames = []
            for k, v in self.__discovered_services.items():
                if v is None:
                    hostnames.append(k)
        for hostname in hostnames:
            self.__resolve(hostname)

    def __resolve(self, hostname):
        try:
            interface, protocol, hostname, type, domain, host, aprotocol, address, port, txt, flags = self.__avahi_server.ResolveService(self.IF_UNSPEC, self.PROTO_INET, hostname, self.__SERVICE_TYPE, "local", -1, 0)
        except:
            # Failed to resolve the service. This can happen, and we want to try again unless it gets removed by the RemoveService callback.
            return
        with self.__discovered_services_lock:
            if hostname not in self.__discovered_services:
                # Name was removed from services while we where resolving the service
                return
            self.__discovered_services[hostname] = address
            if re.match("^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$", address) is None:
                log.warning("Got strange IP address for: %s:%s", hostname, address)
                return
            self.__updatePrinterAddress(hostname, address)

    def __updatePrinterAddress(self, hostname, address):
        try:
            data = Ultimaker3API(address, "", "").get("api/v1/system").json()
        except IOError:
            return
        with database.Session() as s:
            guid = data.get("guid", hostname)   # Older firmware does not expose the guid yet, so fall back to hostnames for those. In the future we should reject these printers.
            name = data.get("name", hostname)
            printer = s.query(database.Printer).filter_by(guid=guid).first()
            if printer is not None:
                log.info("Printer IP changed: %s:%s:%s", hostname, address, name)
                # Update an IP address of a printer in this cluster.
                printer.ip_address = address
            else:
                log.debug("New printer discovered: %s:%s:%s", hostname, address, name)
                # Printer not in cluster, add/update it to the discovered printers table.
                with database.Session() as s:
                    discovered_printer_record = s.query(database.DiscoveredPrinter).filter_by(hostname=hostname).first()
                    if discovered_printer_record is not None:
                        discovered_printer_record.ip_address = address
                    else:
                        discovered_printer_record = database.DiscoveredPrinter(name=name, hostname=hostname, guid=guid, ip_address=address, state="discovered")
                        s.add(discovered_printer_record)

    def __removePrinter(self, hostname):
        log.debug("Printer gone missing: %s", hostname)
        with database.Session() as s:
            discovered_printer_record = s.query(database.DiscoveredPrinter).filter_by(hostname=hostname).first()
            if discovered_printer_record is not None:
                s.delete(discovered_printer_record)

    # When this plugin gets deleted, we are shutting down. We want to stop the discovery so avahi removes the service browser we created. Else we leak resources in avahi.
    def __del__(self):
        self.stop()
