from ct.core import converter
from ct.core import database


class DiscoveredPrinterToApiOutput(converter.Converter):
    def __init__(self):
        super().__init__(database.DiscoveredPrinter, dict)

    def handleConversion(self, input):
        return {
            "name": input.name,
            "guid": input.guid,
            "ip_address": input.ip_address,
            "state": input.state,
        }
