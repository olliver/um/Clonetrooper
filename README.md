
Installing Python Dependencies
------------------------------
Run `pip install -r requirements.txt`


Building the front end code
---------------------------

* First, ensure that you have a reasonable recent Nodejs installed. https://nodejs.org/en/ This is needed for the build tools.
* In the Clonetrooper root do `npm install` to install the needed tools.
* And `npm run build-all` to build the code and CSS.
* The final output is dumped in `static/bundle.js`.

`npm run build` is enough to just build the TypeScript code while skipping the big CSS build.

Running the server code
---------------------------
For development, you can start the server code with "python3 -m ct.main" from the root path of this repository.


Development - Python
--------------------
The python code uses:
Flask: Webserver: http://flask.pocoo.org/docs/0.12/
SQLAlchemy: Database mapping&abstraction: http://docs.sqlalchemy.org/en/rel_1_1/orm/tutorial.html
