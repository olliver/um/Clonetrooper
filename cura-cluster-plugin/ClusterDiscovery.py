
from UM.Application import Application
from UM.Extension import Extension
from UM.Logger import Logger
from UM.PluginRegistry import PluginRegistry

from UM.Signal import Signal

from PyQt5.QtCore import QObject, QUrl, pyqtSlot, pyqtProperty, pyqtSignal
from PyQt5.QtQml import QQmlComponent, QQmlContext

from UM.Preferences import Preferences

import os

from UM.i18n import i18nCatalog
i18n_catalog = i18nCatalog("cura")

import time


class ClusterDiscovery(QObject, Extension):
    def __init__(self,  parent = None):
        super().__init__(parent)
        self.addMenuItem(i18n_catalog.i18n("Find clusters"), self.showDiscoveryPopup)
        self._view = None
        self._clusters = {}
        self._zero_conf = None
        self._browser = None

        self._cluster_plugin = None

    clustersChanged = pyqtSignal()

    def showDiscoveryPopup(self):
        if self._view is None:
            self._createView()
        self._view.show()

    ##  Creates the view . The view is saved because of the fairly aggressive garbage collection.
    def _createView(self):
        Logger.log("d", "Creating find cluster plugin view.")

        path = QUrl.fromLocalFile(os.path.join(PluginRegistry.getInstance().getPluginPath("cura-cluster-plugin"), "ClusterDiscovery.qml"))
        self._component = QQmlComponent(Application.getInstance()._engine, path)

        # We need access to engine (although technically we can't)
        self._context = QQmlContext(Application.getInstance()._engine.rootContext())
        self._context.setContextProperty("manager", self)
        self._view = self._component.create(self._context)
        Logger.log("d", "Find cluster view view created.")

    def _onClusterDiscoveryChanged(self, *args):
        self.clustersChanged.emit()

    @pyqtSlot(str)
    def setActiveCluster(self, cluster_id):
        Preferences.getInstance().setValue("cluster/active_cluster", cluster_id)
        if self._cluster_plugin:
            self._cluster_plugin.reCheckConnections()

    @pyqtSlot()
    def startDiscovery(self):
        if not self._cluster_plugin:
            self._cluster_plugin = Application.getInstance().getOutputDeviceManager().getOutputDevicePlugin("cura-cluster-plugin")
            self._cluster_plugin.clusterListChanged.connect(self._onClusterDiscoveryChanged)
            self.clustersChanged.emit()

    @pyqtSlot()
    def restartDiscovery(self):
        if not self._cluster_plugin:
            self.startDiscovery()
        else:
            self._cluster_plugin.startDiscovery()

    @pyqtProperty("QVariantList", notify=clustersChanged)
    def foundClusters(self):
        if self._cluster_plugin:
            clusters = list(self._cluster_plugin.getClusters().values())
            return clusters
        else:
            return []