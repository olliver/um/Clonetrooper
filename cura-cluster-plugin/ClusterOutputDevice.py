# Copyright (c) 2017 Ultimaker B.V.

from UM.OutputDevice.OutputDevice import OutputDevice
from UM.Application import Application
from UM.Message import Message
from UM.Logger import Logger

from PyQt5.QtNetwork import QHttpMultiPart, QHttpPart, QNetworkRequest, QNetworkAccessManager, QNetworkReply
from PyQt5.QtCore import QUrl, pyqtSignal, pyqtProperty, QObject, pyqtSlot, QCoreApplication, QTimer

import cura.Settings.ExtruderManager
from PyQt5.QtWidgets import QMessageBox


from UM.i18n import i18nCatalog

import gzip
import json

catalog = i18nCatalog("cura")


##  Implements an OutputDevice that supports saving to print clusters
class ClusterOutputDevice(QObject, OutputDevice):
    def __init__(self, id, adress, properties, parent = None):
        super().__init__(device_id = id, parent = parent)

        self._name = properties.get(b"name", b"").decode("utf-8")
        if self._name == "":
            self._name = id
        # Setup output device info
        self.setShortDescription(catalog.i18nc("@action:button Preceded by 'Ready to'.", "Send to cluster (%s)" % self._name))
        self.setIconName("print")
        self.setName(self._name)
        self.setDescription(catalog.i18nc("@info:tooltip", "Send the sliced model(s) to the cluster"))

        self._gcode = None
        self._progress_message = None

        self._post_multi_part = None
        self._post_request = None
        self._post_part = None
        self._manager = None

        self._address = adress
        self._api_prefix = ":81/api/v1/cluster/"
        self._id = id
        self._properties = properties

        self._use_gzip = True  # Should we use g-zip compression before sending the data?

        self._update_timer = QTimer()
        self._update_timer.setInterval(2000)  # TODO; Add preference for update interval
        self._update_timer.setSingleShot(False)
        self._update_timer.timeout.connect(self._update)

        self._printers_list_json = []
        self._printer_configurations = []

        self._num_extruders = 2

    # Signal that is emitted every time connection state is changed.
    # it also sends it's own device_id (for convenience sake)
    connectionStateChanged = pyqtSignal(str)

    ## IPadress of this cluster
    @pyqtProperty(str, constant=True)
    def ipAddress(self):
        return self._address

    ##  Name of the printer (as returned from the ZeroConf properties)
    @pyqtProperty(str, constant=True)
    def name(self):
        return self._name

    @pyqtSlot(result = str)
    def getKey(self):
        return self._id

    def isConnected(self):
        # TODO: Hardcoded now, but this needs to change.
        return True

    def setConnectionState(self, state):
        self.connectionStateChanged.emit(self._id)

    def startPrint(self):
        # Notify user that we are sending data
        self._progress_message = Message(catalog.i18nc("@info:status", "Sending data to cluster"), 0, False, -1)
        self._progress_message.addAction("Abort", catalog.i18nc("@action:button", "Cancel"), None, "")
        self._progress_message.actionTriggered.connect(self._progressMessageActionTrigger)
        self._progress_message.show()

        ## Mash the data into single string
        byte_array_file_data = b""
        for line in self._gcode:

            if self._use_gzip:
                byte_array_file_data += gzip.compress(line.encode("utf-8"))
                QCoreApplication.processEvents()  # Ensure that the GUI does not freeze.
                # Pretend that this is a response, as zipping might take a bit of time.
            else:
                byte_array_file_data += line.encode("utf-8")

        # Actually send the data.
        if self._use_gzip:
            file_name = "%s.gcode.gz" % Application.getInstance().getPrintInformation().jobName
        else:
            file_name = "%s.gcode" % Application.getInstance().getPrintInformation().jobName

        self._post_multi_part = QHttpMultiPart(QHttpMultiPart.FormDataType)

        ##  Create part (to be placed inside multipart)
        self._post_part = QHttpPart()
        self._post_part.setHeader(QNetworkRequest.ContentDispositionHeader,
                                  "form-data; name=\"file\"; filename=\"%s\"" % file_name)
        self._post_part.setBody(byte_array_file_data)
        self._post_multi_part.append(self._post_part)

        url = QUrl("http://" + self._address + self._api_prefix + "jobs")

        ##  Create the QT request
        self._post_request = QNetworkRequest(url)

        ##  Post request + data
        self._post_reply = self._manager.post(self._post_request, self._post_multi_part)
        self._post_reply.uploadProgress.connect(self._onUploadProgress)


    def requestWrite(self, nodes, file_name = None, limit_mimetypes = False, file_handler = None):
        self.writeStarted.emit(self)
        self._gcode = getattr(Application.getInstance().getController().getScene(), "gcode_list")

        print_information = Application.getInstance().getPrintInformation()

        text = catalog.i18nc("@label", "Are you sure you want to send this print to the cluster?")
        informative_text = catalog.i18nc("@label",
                                              "None of the printers in the cluster has the currently selected configuration."
                                              "The cluster can not start the current print right away. A change in configuration / materials is required.")

        local_materials = [""] * self._num_extruders
        local_hotend_types = [""] * self._num_extruders
        extruder_manager = cura.Settings.ExtruderManager.ExtruderManager.getInstance()
        for index in range(0, self._num_extruders):
            if print_information.materialLengths[index] == 0:
                local_materials[index] = "*"
                local_hotend_types[index] = "*"
                continue
            material = extruder_manager.getExtruderStack(index).findContainer({"type": "material"})
            if material:
                local_materials[index] = material.getMetaDataEntry("GUID")

            variant = extruder_manager.getExtruderStack(index).findContainer({"type": "variant"})
            if variant:
                local_hotend_types[index] = variant.getName()



        local_printer = {"materials": local_materials, "hotend_types": local_hotend_types}

        matching_printer = False
        for printer in self._printer_configurations:
            current_printer_matches = True
            for index in range(0, self._num_extruders):
                if local_printer["materials"][index] == "*":
                    continue  # Don't need to check as this extruder is not used.

                if printer["materials"][index] != local_printer["materials"][index]:
                    current_printer_matches = False
                    break

                if printer["hotend_types"][index] != local_printer["hotend_types"][index]:
                    current_printer_matches = False
                    break

            if current_printer_matches:
                matching_printer = True
                break

        if not matching_printer:
            detailed_text = ""
            Application.getInstance().messageBox(catalog.i18nc("@window:title", "Cluster configuration mismatch"),
                                                 text,
                                                 informative_text,
                                                 detailed_text,
                                                 buttons=QMessageBox.Yes + QMessageBox.No,
                                                 icon=QMessageBox.Question,
                                                 callback=self._configurationMismatchMessageCallback
                                                 )
            return
        self.startPrint()

    def _configurationMismatchMessageCallback(self, button):
        def delayedCallback():
            if button == QMessageBox.Yes:
                self.startPrint()
        # For some unknown reason Cura on OSX will hang if we do the call back code
        # immediately without first returning and leaving QML's event system.
        QTimer.singleShot(100, delayedCallback)


    def _progressMessageActionTrigger(self, message_id = None, action_id = None):
        if action_id == "Abort":
            Logger.log("d", "User aborted sending print to remote.")

            if self._progress_message:
                self._progress_message.hide()

            if self._post_reply:
                self._post_reply.abort()
                self._post_reply = None

    def _onUploadProgress(self, bytes_sent, bytes_total):
        if bytes_total > 0:
            new_progress = bytes_sent / bytes_total * 100
            if new_progress > self._progress_message.getProgress():
                self._progress_message.show()  # Ensure that the message is visible.
                self._progress_message.setProgress(bytes_sent / bytes_total * 100)
        else:
            self._progress_message.setProgress(0)
            self._progress_message.hide()

    ##  Start requesting data from printer
    def connect(self):
        self.close()  # Ensure that previous connection (if any) is killed.

        self._createNetworkManager()

        # TODO: DEBUG CODE
        self.setConnectionState("test")

        self._update_timer.start()

    def close(self):
        # Stop update timers
        self._update_timer.stop()
        pass

    def _update(self):
        # Get information about all the printers of the cluster
        url = QUrl("http://" + self._address + self._api_prefix + "printers")
        printer_request = QNetworkRequest(url)
        self._manager.get(printer_request)

    def _createNetworkManager(self):
        if self._manager:
            self._manager.finished.disconnect(self._onFinished)

        self._manager = QNetworkAccessManager()
        self._manager.finished.connect(self._onFinished)

    def _updatePrintersConfiguration(self):
        self._printer_configurations = []
        for printer in self._printers_list_json:
            num_hotends = int(printer["properties"]["HOTEND_COUNT"])
            materials = [""] * num_hotends
            hotend_types = [""] * num_hotends
            for idx in range(0, num_hotends):
                materials[idx] = printer["properties"]["HOTEND[%s]MATERIAL" % idx]
                hotend_types[idx] = printer["properties"]["HOTEND[%s]TYPE_ID" % idx]
            self._printer_configurations.append({"materials": materials, "hotend_types": hotend_types})

    ##  Handler for all requests that have finished.
    def _onFinished(self, reply):
        reply_url = reply.url().toString()
        if reply.operation() == QNetworkAccessManager.GetOperation:
            if "printers" in reply_url:
                try:
                    self._printers_list_json = json.loads(bytes(reply.readAll()).decode("utf-8"))
                except json.decoder.JSONDecodeError:
                    pass
                self._updatePrintersConfiguration()

        Logger.log("d", "Cluster got a reply: %s from %s", reply.attribute(QNetworkRequest.HttpStatusCodeAttribute), reply_url)
        pass