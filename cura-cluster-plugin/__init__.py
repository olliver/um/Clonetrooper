 # Copyright (c) 2017 Ultimaker B.V.

from UM.i18n import i18nCatalog
catalog = i18nCatalog("cura")

from . import ClusterOutputPlugin
from . import ClusterDiscovery

def getMetaData():
    return {
        "plugin": {
            "name": "Cluster",
            "author": "Ultimaker",
            "version": "1.0",
            "description": "Cluster",
            "api": 3
        }
    }


def register(app):
    return {"output_device": ClusterOutputPlugin.ClusterOutputPlugin(), "extension": ClusterDiscovery.ClusterDiscovery()}
