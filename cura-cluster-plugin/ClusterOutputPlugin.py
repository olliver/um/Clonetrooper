from UM.OutputDevice.OutputDevicePlugin import OutputDevicePlugin

from . import ClusterOutputDevice

from zeroconf import Zeroconf, ServiceBrowser, ServiceStateChange, ServiceInfo
from UM.Logger import Logger
from UM.Signal import Signal

from UM.Preferences import Preferences

import time

class ClusterOutputPlugin(OutputDevicePlugin):
    def __init__(self):
        super().__init__()
        self._browser = None
        self._clusters = {}
        self._zero_conf = None
        self.addClusterSignal.connect(self.addCluster)
        Preferences.getInstance().addPreference("cluster/active_cluster", "")

    clusterListChanged = Signal()

    def getClusters(self):
        return self._clusters

    def start(self):
        self.startDiscovery()
        #self.getOutputDeviceManager().addOutputDevice(ClusterOutputDevice.ClusterOutputDevice())

    def reCheckConnections(self):
        active_cluster_id = Preferences.getInstance().getValue("cluster/active_cluster")
        if not active_cluster_id:
            return

        for key in self._clusters:
            if key == active_cluster_id:
                Logger.log("d", "Connecting cluster: [%s]..." % key)
                self._clusters[key].connect()
                self._clusters[key].connectionStateChanged.connect(self._onClusterConnectionStateChanged)
            else:
                if self._clusters[key].isConnected():
                    Logger.log("d", "Closing connection of cluster: [%s]..." % key)
                    self._clusters[key].close()

    ##  Handler for zeroConf detection
    def _onServiceChanged(self, zeroconf, service_type, name, state_change):
        if state_change == ServiceStateChange.Added:
            Logger.log("d", "Found cluster: %s" % name)
            info = ServiceInfo(service_type, name, properties={})
            for record in zeroconf.cache.entries_with_name(name.lower()):
                info.update_record(zeroconf, time.time(), record)

            for record in zeroconf.cache.entries_with_name(info.server):
                info.update_record(zeroconf, time.time(), record)
                if info.address:
                    break

            # Request more data if info is not complete
            if not info.address:
                Logger.log("d", "Trying to get address of %s", name)
                info = zeroconf.get_service_info(service_type, name)
            if info:
                type_of_device = info.properties.get(b"type", None)
                if type_of_device == b"Clonetrooper":
                    address = '.'.join(map(lambda n: str(n), info.address))
                    self.addClusterSignal.emit(str(name), address, info.properties)

    addClusterSignal = Signal()

    ##  Handler for when the connection state of one of the detected clusters changes
    def _onClusterConnectionStateChanged(self, key):
        print("on cluster connection state changed", key)
        if key not in self._clusters:
            return

        if self._clusters[key].isConnected():
            self.getOutputDeviceManager().addOutputDevice(self._clusters[key])
        else:
            self.getOutputDeviceManager().removeOutputDevice(key)

    ##  Because the model needs to be created in the same thread as the QMLEngine, we use a signal.
    def addCluster(self, name, address, properties):
        cluster = ClusterOutputDevice.ClusterOutputDevice(name, address, properties)
        self._clusters[cluster.getKey()] = cluster

        active_cluster = Preferences.getInstance().getValue("cluster/active_cluster")
        cluster.connectionStateChanged.connect(self._onClusterConnectionStateChanged)
        if cluster.getKey() == active_cluster:

            cluster.connect()
        self.clusterListChanged.emit()

    def startDiscovery(self):
        self.stop()
        if self._browser:
            self._browser.cancel()
            self._browser = None
            self._clusters = {}
            self.clusterListChanged.emit()
        # After network switching, one must make a new instance of Zeroconf
        self._zero_conf = Zeroconf()
        self._browser = ServiceBrowser(self._zero_conf, u'_um-cluster._tcp.local.', [self._onServiceChanged])

    def stop(self):
        if self._zero_conf is not None:
            Logger.log("d", "zeroconf close...")
            self._zero_conf.close()