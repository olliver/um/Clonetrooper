## @package dbusif
# Documentation on using dbusif for DBus service implementers and users.
#
# DBus is a message bus running. There can be multiple buses; an Ubuntu Linux system usually has a System-bus and a Session-bus.
# Services based on griffin.dbusif run on the System-bus.
# For this, services need some permissions to be allowed on the bus.
# The file <a href="../system_config/dbus/nl.ultimaker.griffin.conf">opinicus/system_config/dbus/nl.ultimaker.griffin.conf</a> provides an example for this.
# Such a configuration file must be added to /etc/dbus-1/system.d
#
# Implementing a service
# ======================
# A service is implemented by an object that inherits from dbusif.ServiceObject():
# \code{.py}
# class SomeService(dbusif.ServiceObject):
#     def __init__(self):
#         super(SomeService, self).__init__("some")
# \endcode
#
# The methods that must be available on DBus must be marked with the dbusif.method()-decorator.
# The name of the method in Python is also the name of the method on DBus.
# The interface of a method is specified with strings specifying the input and output types:
# \code{.py}
#     @dbusif.method("ss", "si")
#     def do_thing(self, string1, string2):
#         return string1+string2, 42
# \endcode
# Decorating with \@dbusif.method("ss", "si") indicates that the method has 2 "s"trings as input and a string and integer as output.
# Please see https://dbus.freedesktop.org/doc/dbus-specification.html for details.
#
# DBus also supports signals. These are declared in a similar manner, by marking a Python-method with the dbusif.signal()-decorator:
# \code{.py}
#     @dbusif.signal("s")
#     def stuffHappened(self, stuff_name):
#         return True
#
#     def wait_for_stuff_to_happen(self):
#         wait()
#         self.stuff_happened("I forgot my towel")
# \endcode
# Decorating with \@dbusif.signal("s") indicates that a signal has a string-parameter.
# The name of the decorated method is the name of the event on DBus.
#
# To run a service, instantiate the object and run dbusif.runMainLoop().
# <a href="../main.py">main.py in the Opinicus root directory</a> provides some helpers for this.
#
# Using a service
# ===============
#
# In order to use a service, you need a dbusif.RemoteObject() with the correct name of the back-end service:
# Then, you can call methods on the someService-object as if the service was a normal Python object.
# \code{.py}
# someService = dbusif.RemoteObject(name="some")
# greet, answer = someService.do_thing("Hello ", "World")
# \endcode
#
# Subscribing to a signal is done via someService.connectSignal("signalName", callback_function).
# For signals to work, the DBus main loop must be running (with dbusif.runMainLoop() )
# \code{.py}
# def handleStuffHappening(stuff):
#     print(stuff)
# someService.connectSignal("stuffHappened", handleStuffHappening)
# dbusif.runMainLoop()
# \endcode

import dbus.service
import dbus.mainloop.glib
from gi.repository import GLib

import threading
import logging
import time
import functools


log = logging.getLogger(__name__.split(".")[-1])

# Maximum call time allowed for d-bus calls to be busy.
# If a dbus method on a service object takes longer than the specified amount time to do its job, a log message is written.
# Set to None to disable these checks.
__DBUS_MAXIMUM_CALL_TIME = 0.1

# Assistance module to help in working with the dbus-python bindings
#  This module abstracts the dbus bindings to be easier, and to be better thread safe.
#  As the normal use of the dbus bindings with threads can deadlock quite easy.


# Initialize gobject and glib for threads, needs to be done before any dbus call is made.
# Which is why it is done during module load.
GLib.threads_init()
dbus.mainloop.glib.threads_init()

# Global variables that track the state of the dbus bindings.
# we keep track of which is the main loop thread, so we can check if calls are done from the main event loop or need to
# be pushed on the main loop to prevent deadlocks.
_main_loop_thread = None
_loop = GLib.MainLoop()
# Use a single bus object for all dbus communication.
_bus = dbus.SystemBus(private=True, mainloop=dbus.mainloop.glib.DBusGMainLoop())
# General lock around calls on dbus remote objects, as calling from multiple threads seems to cause the dbus-python to get confused and give timeouts.
_lock = threading.RLock()

# Start running the main event loop. Will run until "abortMainLoop" is called from another thread.
def runMainLoop():
    global _main_loop_thread
    _main_loop_thread = threading.current_thread()
    _loop.run()


# Abort the main loop running in runMainLoop
def abortMainLoop():
    GLib.idle_add(_loop.quit)


class ServiceObject(dbus.service.Object):
    def __init__(self, name, path=None):
        global _bus
        if path is None:
            path = name
        super().__init__(dbus.service.BusName("nl.ultimaker." + name, _bus), "/nl/ultimaker/" + path)

    @dbus.service.method("nl.ultimaker", "s", "s")
    def debugExecCode(self, code):
        result = eval(code, {}, {"self": self})
        return repr(result)


# Factory for decorators used to mark methods of a `ServiceObject` to be exported on the D-Bus.
# Currently, we default to the 'nl.ultimaker' interface when no interface is supplied. While technically
# correct, this yields a method like 'nl.ultimaker.blink' for the led object method 'blink'.
# "Best practice" however is to have your object be reflected in the interface, and thus
# nl.ultimaker.led.Blink would be the correct way.
def method(in_signature=None, out_signature=None, interface="nl.ultimaker"):
    global __DBUS_MAXIMUM_CALL_TIME
    if __DBUS_MAXIMUM_CALL_TIME is not None:
        # In case we have a maximum call time, we need to wrap the dbus call in another layer of decoration.
        # As the dbus decorator adds extra attributes to the function, we need to copy this, we also need to make sure
        # that the dbus decorator "sees" the right parameters.
        dbus_decorator_generator = dbus.service.method(interface, in_signature, out_signature)
        def wrapper(func):
            result_func = dbus_decorator_generator(func)
            @functools.wraps(result_func)
            def dbus_timing_checker(*args, **kwargs):
                # Actual call, do a measurement on how long the call took and report this when it was longer then the specified maximum time.
                start_time = time.perf_counter()
                result = result_func(*args, **kwargs)
                call_time = time.perf_counter() - start_time
                if call_time > __DBUS_MAXIMUM_CALL_TIME:
                    log.warning("DBus call took longer than allowed: %f: %s(%s, %s)", call_time, func, args, kwargs)
                return result
            # Copy the attributes that the dbus_decorator_generator added to our original resulting function to our new dbus_timing_checker,
            # else the ServiceObject class cannot find these functions and does not expose them through dbus.
            for key in dir(result_func):
                if key.startswith("_dbus"):
                    setattr(dbus_timing_checker, key, getattr(result_func, key))
            return dbus_timing_checker
        return wrapper
    else:
        return dbus.service.method(interface, in_signature, out_signature)


# Factory for decorators used to mark methods of a `ServiceObject` to emit signals on the D-Bus.
# *See interface comment above for method.
def signal(signature=None, interface="nl.ultimaker"):
    return dbus.service.signal(interface, signature)


# Internally used _ProxyMethod object, used to make dbus calls from the RemoteObject class.
# This object handles the offloading of the call to the main event thread, and waiting for a response.
class _ProxyMethod(object):
    def __init__(self, obj_method):
        self.__obj_method = obj_method

    def __call__(self, *args, **kwargs):
        with _lock:
            return self.__obj_method(*args, **kwargs)

    def callLater(self, *args, **kwargs):
        GLib.idle_add(self.callAsync, *args, **kwargs)

    def callAsync(self, *args, result_handler=None):
        with _lock:
            self.__obj_method.call_async(*args, reply_handler=result_handler, error_handler=None)


# Wrapper around dbus objects.
#  Uses the global _bus object to find objects on the bus, and allows the caller to call methods on this object by the use of proxy objects.
class RemoteObject(object):
    def __init__(self, name, path=None, interface=None):
        self._interface = interface
        self._connected_signals = []
        if path is None:
            path = "/nl/ultimaker/" + name
        if not path.startswith("/"):
            path = "/nl/ultimaker/" + path
        if "." not in name:
            name = "nl.ultimaker." + name.replace("/", ".")
        try:
            self.__obj = _bus.get_object(name, path)
        except dbus.exceptions.DBusException as e:
            if e.get_dbus_name() == "org.freedesktop.DBus.Error.ServiceUnknown":
                time.sleep(1.0)
                self.__obj = _bus.get_object(name, path)

    def connectSignal(self, signal_name, function):
        signal = self.__obj.connect_to_signal(signal_name, function)
        self._connected_signals.append(signal)

    def cleanSignals(self):
        for signal in self._connected_signals:
            signal.remove()

    def __getattr__(self, member):
        if member.startswith("__") and member.endswith("__"):
            raise AttributeError(member)
        else:
            return _ProxyMethod(self.__obj.get_dbus_method(member, self._interface))
