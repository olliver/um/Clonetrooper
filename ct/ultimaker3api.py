# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import requests
import json

## Ultimaker 3 API access class.
#  Allows for access of the Ultimaker 3 API with authentication.
#  Uses the python requests library to do the actual http requests, which does most of the work for us.
class Ultimaker3API:
    # @param ip: IP address of the printer
    # @param application: name of the application in string form, used during authentication requests and is shown on the printer.
    def __init__(self, ip, auth_id, auth_key):
        self.__ip = ip
        self.__session = requests.sessions.Session()
        self.__auth = requests.auth.HTTPDigestAuth(auth_id, auth_key)
    
    # Do a new HTTP request to the printer. It formats data as JSON, and fills in the IP part of the URL.
    def request(self, method, path, **kwargs):
        if "data" in kwargs:
            kwargs["data"] = json.dumps(kwargs["data"])
            if "headers" not in kwargs:
                kwargs["headers"] = {"Content-type": "application/json"}
        if "timeout" not in kwargs:
            kwargs["timeout"] = 15
        return self.__session.request(method, "http://%s/%s" % (self.__ip, path), auth=self.__auth, **kwargs)

    # Shorthand function to do a "GET" request.
    def get(self, path, **kwargs):
        return self.request("get", path, **kwargs)

    # Shorthand function to do a "PUT" request.
    def put(self, path, **kwargs):
        return self.request("put", path, **kwargs)

    # Shorthand function to do a "POST" request.
    def post(self, path, **kwargs):
        return self.request("post", path, **kwargs)
