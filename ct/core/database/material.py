import sqlalchemy
import sqlalchemy.orm
import re
from . import _base


## Material record.
class Material(_base.Base):
    __tablename__ = "material"

    id = sqlalchemy.Column(sqlalchemy.Integer, primary_key=True)
    guid = sqlalchemy.Column(sqlalchemy.String, index=True)
    brand = sqlalchemy.Column(sqlalchemy.String)
    material = sqlalchemy.Column(sqlalchemy.String)
    color = sqlalchemy.Column(sqlalchemy.String)

    @staticmethod
    def fromXml(xml_string):
        material = Material()
        # If I see this code in production I will find you and destroy you.
        material.guid = re.search("<GUID>(.*)</GUID>", xml_string).group(1)
        material.brand = re.search("<brand>(.*)</brand>", xml_string).group(1)
        material.material = re.search("<material>(.*)</material>", xml_string).group(1)
        material.color = re.search("<color>(.*)</color>", xml_string).group(1)
        return material
