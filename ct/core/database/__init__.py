from . import _base

# We import all the database definitions here, so the tables get created when we import this module.
# And we make these classes available as "database.TableName"
from .printer import Printer
from .printerProperty import PrinterProperty
from .printJob import PrintJob
from .printJobProperty import PrintJobProperty
from .discoveredPrinter import DiscoveredPrinter
from .material import Material

Session = _base.Session

_base.Base.metadata.create_all(_base.engine)
