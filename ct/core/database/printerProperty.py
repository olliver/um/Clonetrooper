import sqlalchemy
import sqlalchemy.orm
from . import _base


class PrinterProperty(_base.Base):
    __tablename__ = "printer_property"

    printer_id = sqlalchemy.Column(sqlalchemy.Integer, sqlalchemy.ForeignKey('printer.id'), index=True, primary_key=True)
    key = sqlalchemy.Column(sqlalchemy.String, primary_key=True)
    value = sqlalchemy.Column(sqlalchemy.String)

    printer = sqlalchemy.orm.relationship("Printer", back_populates="properties")
