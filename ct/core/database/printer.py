import sqlalchemy
import sqlalchemy.orm
from ct import ultimaker3api
from . import _base
from . import printerProperty


## Representation of a Printer in this cluster
class Printer(_base.Base):
    __tablename__ = "printer"

    id = sqlalchemy.Column(sqlalchemy.Integer, primary_key=True)
    name = sqlalchemy.Column(sqlalchemy.String)
    guid = sqlalchemy.Column(sqlalchemy.String, index=True)
    ip_address = sqlalchemy.Column(sqlalchemy.String)
    state = sqlalchemy.Column(sqlalchemy.String, index=True)

    api_auth_id = sqlalchemy.Column(sqlalchemy.String)
    api_auth_key = sqlalchemy.Column(sqlalchemy.String)

    # Setup the one-to-one relationship of an active job on a printer.
    current_job_id = sqlalchemy.Column(sqlalchemy.Integer, sqlalchemy.ForeignKey('job.id'), index=True, nullable=True)
    current_job = sqlalchemy.orm.relationship("PrintJob", back_populates="active_printer")

    # Setup the one-to-many relationship of properties on this job
    properties = sqlalchemy.orm.relationship("PrinterProperty", back_populates="printer", cascade="all, delete, delete-orphan")

    def getPropertyValue(self, key):
        for p in self.properties:
            if p.key == key:
                return p.value
        return None

    def setPropertyValue(self, key, value):
        for p in self.properties:
            if p.key == key:
                p.value = value
                return
        self.properties.append(printerProperty.PrinterProperty(key=key, value=value))

    def api(self):
        return ultimaker3api.Ultimaker3API(self.ip_address, self.api_auth_id, self.api_auth_key)
