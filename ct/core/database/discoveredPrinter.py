import sqlalchemy
import sqlalchemy.orm
from ct import ultimaker3api
from . import _base


## Representation of a Printer in this cluster
class DiscoveredPrinter(_base.Base):
    __tablename__ = "discoveredPrinter"

    id = sqlalchemy.Column(sqlalchemy.Integer, primary_key=True)
    name = sqlalchemy.Column(sqlalchemy.String)
    hostname = sqlalchemy.Column(sqlalchemy.String, index=True)
    guid = sqlalchemy.Column(sqlalchemy.String, index=True)
    ip_address = sqlalchemy.Column(sqlalchemy.String)
    state = sqlalchemy.Column(sqlalchemy.String, index=True)

    api_auth_key = sqlalchemy.Column(sqlalchemy.String)
    api_auth_id = sqlalchemy.Column(sqlalchemy.String)

    def api(self):
        return ultimaker3api.Ultimaker3API(self.ip_address, "", "")
