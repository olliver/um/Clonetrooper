import sqlalchemy
import sqlalchemy.orm
from . import _base


class PrintJobProperty(_base.Base):
    __tablename__ = "job_property"

    job_id = sqlalchemy.Column(sqlalchemy.Integer, sqlalchemy.ForeignKey('job.id'), index=True, primary_key=True)
    key = sqlalchemy.Column(sqlalchemy.String, primary_key=True)
    value = sqlalchemy.Column(sqlalchemy.String)

    job = sqlalchemy.orm.relationship("PrintJob", back_populates="properties")
