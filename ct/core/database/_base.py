from ct import config

import sqlalchemy
from sqlalchemy.ext import declarative
import sqlalchemy.orm
from contextlib import contextmanager

engine = sqlalchemy.create_engine('sqlite:///%s' % (config.DATABASE_STORAGE_PATH))#, echo=True)

Base = declarative.declarative_base()
_Session = sqlalchemy.orm.sessionmaker(bind=engine)


@contextmanager
def Session(**kwargs):
    session = _Session(**kwargs)
    try:
        yield session
        session.commit()
    except:
        session.rollback()
        raise
    finally:
        session.close()
