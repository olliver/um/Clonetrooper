import logging

log = logging.getLogger(__name__.split(".")[-1])


class Converter:
    __instances = {}
    __converting_set = set()

    def __init__(self, input_type, output_type):
        assert isinstance(input_type, type)
        assert isinstance(output_type, type)

        Converter.__instances[(input_type, output_type)] = self

    def handleConversion(self, input):
        raise NotImplementedError

    @staticmethod
    def convert(input_object, output_type):
        assert isinstance(output_type, type)

        if input_object is None:
            return None
        # Prevent recursive conversion.
        if input_object in Converter.__converting_set:
            return None

        key = (type(input_object), output_type)
        try:
            c = Converter.__instances[key]
        except KeyError:
            log.warning("No conversion object found for: %s to %s", type(input_object), output_type)
            return None
        Converter.__converting_set.add(input_object)
        result = c.handleConversion(input_object)
        Converter.__converting_set.remove(input_object)
        return result
