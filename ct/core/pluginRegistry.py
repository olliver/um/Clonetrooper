import os
import logging
import imp

log = logging.getLogger(__name__.split(".")[-1])


class PluginRegistry:
    __instance = None

    def __init__(self):
        assert type(self) is PluginRegistry
        assert PluginRegistry.__instance is None

        self.__plugin_by_type = {}
        self.__plugins_by_class = {}

    def loadPlugins(self, plugin_location):
        log.info("Loading plugins from: %s", plugin_location)
        for plugin_path in os.listdir(plugin_location):
            if not os.path.isdir(os.path.join(plugin_location, plugin_path)):
                continue
            log.info("Loading plugin: %s", plugin_path)

            # Note that this currently intentionally crashes and burns when there is an error in a plugin.
            # So we do not startup with corrupted plugins.
            file, path, desc = imp.find_module(plugin_path, [plugin_location])
            module = imp.load_module(plugin_path, file, path, desc)

            data = module.register()
            for plugin_info in data:
                if plugin_info["type"] not in self.__plugin_by_type:
                    self.__plugin_by_type[plugin_info["type"]] = []
                log.info("Creating plugin object: %s %s", plugin_path, plugin_info["class"].__name__)
                instance = plugin_info["class"]()
                self.__plugin_by_type[plugin_info["type"]].append(instance)
                self.__plugins_by_class[plugin_info["class"]] = instance

    def getPlugins(self, type_name):
        return self.__plugin_by_type.get(type_name, [])

    def getPluginInstance(self, plugin_class):
        return self.__plugins_by_class[plugin_class]

    @staticmethod
    def create():
        PluginRegistry.__instance = PluginRegistry()
        return PluginRegistry.__instance

    @staticmethod
    def getInstance():
        return PluginRegistry.__instance
