from ct.core import pluginRegistry
from ct import thread
import time


class Daemon:
    def __init__(self):
        pass

    def start(self):
        for plugin in pluginRegistry.PluginRegistry.getInstance().getPlugins("background"):
            thread.Thread("Background for: %s" % (plugin), self.__runBackgroundPlugin, args=(plugin,)).start()

    def __runBackgroundPlugin(self, plugin):
        while True:
            plugin.run()
            time.sleep(1)
