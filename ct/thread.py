from threading import Event
from threading import RLock
from threading import Thread as PythonThread
import logging

log = logging.getLogger(__name__.split(".")[-1])


# Thread object which actually runs the thread from the private thread pool.
# This object has the same interface as the threading.Thread objects, but logs exceptions to the proper logging facilities.
# (This could be renamed to Jobs. As that explains better what it really does)
class Thread():
    def __init__(self, name, target, args=(), kwargs=None, daemon=True):
        if kwargs is None:
            kwargs = {}

        self.__name = name
        self.__target = target
        self.__args = args
        self.__kwargs = kwargs
        self.__daemon = daemon
        self.__run_done_event = Event()
        self.__run_done_event.clear()

    def start(self):
        thread = PythonThread(target=self.run, name=self.__name)
        thread.start()

    def run(self):
        try:
            if self.__name is not None:
                log.debug("%s started", self.__name)
            self.__target(*self.__args, **self.__kwargs)
        except:
            log.exception("Exception in thread: %s", self.__name)
        finally:
            if self.__name is not None:
                log.debug("%s finished", self.__name)
            self.__run_done_event.set()

    def join(self, timeout = None):
        self.__run_done_event.wait(timeout)

    def is_alive(self):
        return not self.__run_done_event.is_set()
