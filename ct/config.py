import os

if os.path.isdir("/var/lib/clonetrooper"):
    __base_path = "/var/lib/clonetrooper"
else:
    __base_path = os.path.abspath(os.path.join(os.path.dirname(__file__), ".."))

FILE_STORAGE_PATH = os.path.join(__base_path, "_uploads")
DATABASE_STORAGE_PATH = os.path.join(__base_path, "clonetrooper.db")
