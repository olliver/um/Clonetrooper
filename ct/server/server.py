import flask
import os
from ct import thread
from ct.core import pluginRegistry
import logging

log = logging.getLogger(__name__.split(".")[-1])


class Server:
    def __init__(self):
        self.__server = flask.Flask("ct-server")
        # Do not except files larger then 50MB.
        self.__server.config['MAX_CONTENT_LENGTH'] = 50 * 1024 * 1024
        self.__server.static_folder = os.path.abspath(os.path.join(os.path.dirname(__file__), "..", "..", "static"))

    def start(self):
        self.__server.add_url_rule("/", view_func=self.__forwardToIndex, methods=["GET"])
        for plugin in pluginRegistry.PluginRegistry.getInstance().getPlugins("api"):
            plugin.registerApiEndpoints(self.__server)
        thread.Thread("Webserver", self.__run).start()

    def __forwardToIndex(self):
        return flask.redirect("/static/index.html")

    def __run(self):
        # Disable http request logging.
        logging.getLogger('werkzeug').disabled = True
        log.info("Starting flask webserver.")
        self.__server.run("::", port=81, threaded=True, debug=True, use_reloader=False)
