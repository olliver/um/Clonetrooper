import flask


def endpoint(path, method):
    def _f(func):
        func._api_path = path
        func._api_method = method
        return func
    return _f


def response(json_data, status_code=200):
    return flask.Response(flask.json.dumps(json_data), status=status_code, mimetype="application/json")

def failedResponse():
    return response({"code": 0, "message": "I am really a teapot"}, 418)

class ApiPlugin:
    def registerApiEndpoints(self, server):
        for k in dir(self):
            func = getattr(self, k)
            if hasattr(func, "_api_path") and hasattr(func, "_api_method"):
                server.add_url_rule("/api/v1/cluster/%s" % (func._api_path), endpoint="%s:%s" % (func._api_path, func._api_method), view_func=func, methods=[func._api_method])
