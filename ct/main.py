from .server import server
from .daemon import daemon
from .core import pluginRegistry
from .core import database
try:
    from . import dbusif
except ImportError:
    dbusif = None

import logging
import os


def main():
    logging.basicConfig(format="%(asctime)-15s %(levelname)-8s %(name)-15s %(message)s", level=logging.INFO)
    logging.getLogger("requests").setLevel(logging.WARNING)
    logging.getLogger("urllib3").setLevel(logging.WARNING)

    pr = pluginRegistry.PluginRegistry.create()
    pr.loadPlugins(os.path.join(os.path.dirname(__file__), "..", "plugins"))

    s = server.Server()
    d = daemon.Daemon()
    s.start()
    d.start()

    if dbusif is not None:
        dbusif.runMainLoop()

if __name__ == "__main__":
    main()
