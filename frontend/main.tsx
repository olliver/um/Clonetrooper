/**
 * 
 */
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import * as RestApi from './rest/api';

const POLL_INTERVAL = 5000; // ms
const SCAN_DELAY = 3000; // ms

/* --------------------------------------------------------------------- */

interface PrinterRowProps {
  printer: RestApi.Printer;
  onDeletePrinter: (uuid: string) => void;
}

class PrinterRow extends React.Component<PrinterRowProps, {} > {
  constructor(props) {
    super(props);
  }

  private _handleDeleteButton(ev: React.ChangeEvent<HTMLButtonElement>): void {
    this.props.onDeletePrinter(ev.target.getAttribute("data-guid"));
  }

  render(): JSX.Element {
    return (
        <tr className="mod-printer">
          <td className="lead.printer-name">{this.props.printer.name}</td>
          <td>{this.props.printer.state}</td>
          <td>{this.props.printer.ip_address}</td>
          <td>{this.props.printer.guid}</td>
          <td className="actions"><button data-guid={this.props.printer.guid} onClick={ this._handleDeleteButton.bind(this) }>Remove</button></td>
        </tr>
    );
  }
}

/* --------------------------------------------------------------------- */

interface PrinterListProps {
  printers: RestApi.Printer[];
  onDeletePrinter: (uuid: string) => void;
}

class PrinterList  extends React.Component<PrinterListProps, {}> {
  constructor(props) {
    super(props);
  }

  render(): JSX.Element {
    const printerRows = this.props.printers.map( (printer: RestApi.Printer): JSX.Element =>
      <PrinterRow printer={printer} key={printer.guid} onDeletePrinter={this.props.onDeletePrinter}></PrinterRow> );
    return (
    <div className=".mod-printers">
      <table className="list-view">
        <thead>
          <tr>
            <th className="lead">Printer</th>
            <th>Status</th>
            <th>IP Address</th>
            <th>UUID</th>
            <th></th>
          </tr>
        </thead>
        <tbody className="filterable-container">{printerRows}</tbody>
      </table>
    </div>);
  }  
}

/* --------------------------------------------------------------------- */
interface AddPrinterProps {
  printers: RestApi.Printer[];
}

class AddPrinter  extends React.Component<AddPrinterProps, {}> {
  constructor(props) {
    super(props);
  }

  render(): JSX.Element {
    return (<div>Add Printer</div>);
  }
}

/* --------------------------------------------------------------------- */

interface PrinterTabProps {
  printers: RestApi.Printer[];
  onAddPrinter: (hostname: string, ip:string) => void;
  onDeletePrinter: (uuid: string) => void;
}

interface PrinterTabState {
  mode: PrinterSubTab;
  hostname: string;
  ip: string;
  addPrinterMethod: AddPrinterMethod;
  selectedDiscoveredPrinter: RestApi.Uuid;
  discoveredPrinters: RestApi.DiscoveredPrinter[];
}

enum AddPrinterMethod {
  HOSTNAME,
  IP,
  DISCOVERED
}

enum PrinterSubTab {
  LIST,
  ADD_PRINTER
}

class PrinterTab  extends React.Component<PrinterTabProps, PrinterTabState> {
  constructor(props) {
    super(props);
    this.state = {
      mode: PrinterSubTab.LIST,
      hostname: "",
      ip: "",
      addPrinterMethod: AddPrinterMethod.DISCOVERED,
      selectedDiscoveredPrinter: null,
      discoveredPrinters: []
    };
  }

  goToList(): void {
    this.setState( { mode: PrinterSubTab.LIST } );
  }

  private _handleAddPrinter(): void {
    this.setState( { mode: PrinterSubTab.ADD_PRINTER, ip: "10.183.1.88" } );
    this._startPrinterScan();    
  }

  private _handleAddPrinterMethodChange(ev: React.ChangeEvent<HTMLInputElement>): void {
    this.setState( { addPrinterMethod: parseInt(ev.target.value, 10) } );
  }

  private _handleAddPrinterButton(event): void {
    event.preventDefault();
    let hostname = this.state.hostname;
    let ip = this.state.ip;

    switch (this.state.addPrinterMethod) {
      case AddPrinterMethod.HOSTNAME:
        ip = "";
        break;

      case AddPrinterMethod.IP:
        hostname = "";
        break;

      case AddPrinterMethod.DISCOVERED:
        const selected = this.state.discoveredPrinters.filter( (printer) => printer.guid === this.state.selectedDiscoveredPrinter );
        if (selected.length === 0) {
          return; // oops
        }
        ip = selected[0].ip_address;
        hostname = "";
        break;
    }

    this.props.onAddPrinter(hostname, ip);
    this.setState( { mode: PrinterSubTab.LIST } );
  }

  private _startPrinterScan(): void {
    RestApi.DefaultApiFactory().discoveredprintersScanPost().then( () => {
      this._loadDiscoveredPrinters();
      setTimeout(this._loadDiscoveredPrinters.bind(this), SCAN_DELAY);
    });
  }

  private _loadDiscoveredPrinters(): void {
    RestApi.DefaultApiFactory().discoveredprintersGet().then( (discoveredPrinters: RestApi.DiscoveredPrinter[]): void => {
      this.setState( (prev) => {
        let selectedPrinter: RestApi.Uuid = prev.selectedDiscoveredPrinter;
        if (discoveredPrinters.filter( (printer) => printer.guid === prev.selectedDiscoveredPrinter).length === 0) {
          if (discoveredPrinters.length === 0) {
            selectedPrinter = null;
          } else {
            selectedPrinter = discoveredPrinters[0].guid;
          }
        }
        return { discoveredPrinters: discoveredPrinters, selectedDiscoveredPrinter: selectedPrinter };
      });
    });
  }

  render(): JSX.Element {
    switch (this.state.mode) {
      case PrinterSubTab.LIST:
        return <div>
          <PrinterList printers={this.props.printers} onDeletePrinter={this.props.onDeletePrinter}></PrinterList>
          <button onClick={this._handleAddPrinter.bind(this)}>Add Printer</button>
          </div>;

      case PrinterSubTab.ADD_PRINTER:
        return <div className="simple-form">
            <h1>Add Printer</h1>
            <form>
              <div className="input-row">
                <div className="left"><input type="radio" name="addPrinterMethod" className="inline-button" value={AddPrinterMethod.DISCOVERED} checked={this.state.addPrinterMethod === AddPrinterMethod.DISCOVERED} onChange={this._handleAddPrinterMethodChange.bind(this)} /> Discovered printers:</div>
                <div className="right">
                  <select value={this.state.selectedDiscoveredPrinter} onChange={ ev => this.setState( { selectedDiscoveredPrinter: ev.target.value} ) } disabled={this.state.addPrinterMethod !== AddPrinterMethod.DISCOVERED}>
                    { this.state.discoveredPrinters.map( (printer) => <option key={printer.guid} value={printer.guid}>{printer.name} ({printer.ip_address})</option> ) }
                  </select><button onClick={(event) => { event.preventDefault(); this._startPrinterScan(); } }>Scan</button></div>
              </div>
              
              <div className="input-row">
                <div className="left"><input type="radio" name="addPrinterMethod" className="inline-button" value={AddPrinterMethod.HOSTNAME} checked={this.state.addPrinterMethod === AddPrinterMethod.HOSTNAME} onChange={this._handleAddPrinterMethodChange.bind(this)} />
                Hostname:</div>
                <div className="right"><input type="text" value={this.state.hostname} onChange={ ev => this.setState( { hostname: ev.target.value} ) } disabled={this.state.addPrinterMethod !== AddPrinterMethod.HOSTNAME} /></div>
              </div>

              <div className="input-row">
                <div className="left"><input type="radio" name="addPrinterMethod" className="inline-button" value={AddPrinterMethod.IP} checked={this.state.addPrinterMethod === AddPrinterMethod.IP} onChange={this._handleAddPrinterMethodChange.bind(this)} /> IP:</div>
                <div className="right"><input type="text" value={this.state.ip} onChange={ ev => this.setState( { ip: ev.target.value} ) } disabled={this.state.addPrinterMethod !== AddPrinterMethod.IP} /></div>
              </div>

              <div className="form-actions">
                <button className="submit" onClick={this._handleAddPrinterButton.bind(this)}>Add</button>
              </div>
            </form>
          </div>;

      default:
        return null;
    }
  }
}


/* --------------------------------------------------------------------- */

interface PrintJobProps {
  printJob: RestApi.PrintJob;
}

class PrintJobRow extends React.Component<PrintJobProps, {} > {
  constructor(props) {
    super(props);
  }

  render(): JSX.Element {
    return (
        <tr>
          <td className="job-name">{this.props.printJob.name}</td>
          <td>{this.props.printJob.printer}</td>
          <td>{this.props.printJob.state}</td>
          <td>{this.props.printJob.time_elapsed}</td>
          <td>{this.props.printJob.time_remaining}</td>
        </tr>
    );
  }
}

/* --------------------------------------------------------------------- */

interface PrintJobListProps {
  printJobs: RestApi.PrintJob[];
}
class PrintJobList  extends React.Component<PrintJobListProps, {}> {
  constructor(props) {
    super(props);
  }

  render(): JSX.Element {
    const printerRows = this.props.printJobs.map( (printJob: RestApi.PrintJob): JSX.Element => <PrintJobRow printJob={printJob} key={printJob.uuid}></PrintJobRow> );
    return (<table className="list-view filterable-container">
      <thead>
        <tr>
          <th className="headers name">Print Job</th>
          <th>Printer</th>
          <th className="status">Status</th>
          <th>Time elapsed</th>
          <th>Time remaining</th>
        </tr>
      </thead>
      <tbody>{printerRows}</tbody>
    </table>);
  }  
}


/* --------------------------------------------------------------------- */
enum Tab {
  PRINTERS, JOBS
}

interface AppState {
  tab: Tab;
  printers: RestApi.Printer[];
  printJobs: RestApi.PrintJob[];
}

class App extends React.Component<{}, AppState> {

  private _printerTab: PrinterTab = null;

  private _printerPollInFlight: boolean = false;

  private _printJobPollInFlight: boolean = false;

  constructor(props) {
    super(props);
    this.state = {
      tab: Tab.PRINTERS,
      printers: [],
      printJobs: []
    };
  }

  componentDidMount(): void {
    this.pollPrinters();
    this.pollPrintJobs();
  }

  handleTabPrinters(): void {
    this.setState( prevState => ({ tab: Tab.PRINTERS, printerSubTab: PrinterSubTab.LIST }) );
    this.loadPrinters();
    this._printerTab.goToList();
  }

  loadPrinters(): void {
    if ( ! this._printerPollInFlight) {
      this._printerPollInFlight = true;
      RestApi.DefaultApiFactory().printersGet().then( (printers: RestApi.Printer[]): void => {
        this._printerPollInFlight = false;
        this.setState( (prev) => ({ printers: printers }));
      });
    }
  }

  pollPrinters(): void {
    if (this.state.tab === Tab.PRINTERS) {
      this.loadPrinters();
    }
    setTimeout(this.pollPrinters.bind(this), POLL_INTERVAL);
  }

  handleTabJobQueue(): void {
    this.setState( prevState => ({ tab: Tab.JOBS }) );
    this.loadPrintJobs();
  }

  loadPrintJobs(): void {
    if ( ! this._printJobPollInFlight) {
      this._printJobPollInFlight = true;
      RestApi.DefaultApiFactory().jobsGet().then( (printJobs: RestApi.PrintJob[]): void => {
        console.log("Got print jobs!", printJobs);
        this._printJobPollInFlight = false;
        this.setState( (prev) => ({ printJobs: printJobs }));
      });
    }
  }

  pollPrintJobs(): void {
    if (this.state.tab === Tab.JOBS) {
      this.loadPrintJobs();
    }
    setTimeout(this.pollPrintJobs.bind(this), POLL_INTERVAL);
  }

  private _handleAddPrinter(hostname: string, ip: string): void {
    let payload = {};
    if (hostname != null && hostname.trim() !== "") {
      payload = { hostname: hostname };
    } else {
      payload = { ip: ip };
    }

    RestApi.DefaultApiFactory().printersPost(payload).then( () => {
      this.loadPrinters();
    });
  }

  private _handleDeletePrinter(guid: string): void {
    RestApi.DefaultApiFactory().printersUuidDelete( { uuid: guid } ).then( () => {
      this.loadPrinters();
    });
  }

  render(): JSX.Element {
    return (
      <div>
        <header>
          <div className="top">
            <div className="wrapper">
              <a><img src="ultimaker.svg" className="um-logo" /></a>
              <nav className="main">
                <ul>
                  <li onClick={this.handleTabPrinters.bind(this)}><a><span className={this.state.tab === Tab.PRINTERS ? "current" : ""}>Printers</span></a></li>
                  <li onClick={this.handleTabJobQueue.bind(this)}><a><span className={this.state.tab === Tab.JOBS ? "current" : ""}>Job Queue</span></a></li>
                </ul>
              </nav>
            </div>
          </div>
        </header>
        <div id="container" className="wrapper">
          <main data-filter-state="initialized">
          {this.state.tab === Tab.PRINTERS
            ? <PrinterTab ref={(thisTab: PrinterTab) => { this._printerTab = thisTab; }} printers={this.state.printers}
              onAddPrinter={this._handleAddPrinter.bind(this)} onDeletePrinter={this._handleDeletePrinter.bind(this)}></PrinterTab>
            : <PrintJobList printJobs={this.state.printJobs}></PrintJobList>}
          </main>
        </div>
        <footer>
          <div className="wrapper">
            <div className="mod-application-version">
              <div className="current-version">v1.0.0</div>
            </div>
          </div>
        </footer>
      </div>
    );
  }
}

/* --------------------------------------------------------------------- */

function start(): void {
  console.log("Starting");

  ReactDOM.render(
    <App></App>,
    document.getElementById('root')
  );
}
window.addEventListener("load", start);
